/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

/**
 *
 * @author harshil.janu.solanki
 */
public class RatingClass {
    private int ratId;
    private String ratDesc;
    
    
    public RatingClass(int ratId, String ratDesc)
    {
        this.ratId = ratId;
        this.ratDesc = ratDesc;
    }

    public RatingClass() {
        this.ratId = 0;
        this.ratDesc = "Anonymous";
    }

    /**
     * @return the ratId
     */
    public int getRatId() {
        return ratId;
    }

    /**
     * @param ratId the ratId to set
     */
    public void setRatId(int ratId) {
        this.ratId = ratId;
    }

    /**
     * @return the ratDesc
     */
    public String getRatDesc() {
        return ratDesc;
    }

    /**
     * @param ratDesc the ratDesc to set
     */
    public void setRatDesc(String ratDesc) {
        this.ratDesc = ratDesc;
    }
    
    @Override
    public String toString()
    {
        return "Rating: - "+ " " + this.ratId+ "Rating Description: - " + " " + this.ratDesc;
    }
}
