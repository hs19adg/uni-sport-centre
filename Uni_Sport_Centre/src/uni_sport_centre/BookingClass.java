/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

import java.text.DecimalFormat;

/**
 *
 * @author harshil.janu.solanki
 */
public class BookingClass {
      //Private fucntion for student
    private  StudentClass stud = new StudentClass();
    //Private function for excercise/activity
    private ExcercisesClass act = new ExcercisesClass();
    //Private function for timetable
    private TimetableClass timeact = new TimetableClass();
    //Private function for rating
    private RatingClass rating = new RatingClass();
    //Private function for review
    private ReviewClass reviews = new ReviewClass();
    
    
    public BookingClass(StudentClass studRec, ExcercisesClass actRec, TimetableClass actTimetab, RatingClass rateRec, ReviewClass reviewAct)
    {
        //Student
        stud.setStuId(studRec.getStuId());
        stud.setStuName(studRec.getStuName());
        stud.setStuAge(studRec.getStuAge());
        //act.setActId(actRec.getActId());
        //Activity
        //act.setActName(actRec.getActName());
        act.setActName(actRec.getActName(), true);
        //act.setActPrice(actRec.getActPrice());
        //Timetable
        timeact.setActDay(actTimetab.getActDay());
        timeact.setActShift(actTimetab.getActShift());
        //Rating
        rating.setRatId(rateRec.getRatId());
        rating.setRatDesc(rateRec.getRatDesc());
        //Review
        reviews.setrId(reviewAct.getrId());
        reviews.setrComment(reviewAct.getrComment());
       
    }
    
    public BookingClass()
    {
        stud.getStuId();
        stud.getStuName();
        stud.getStuAge();
        act.getActName();
        timeact.getActDay();
        timeact.getActShift();
        rating.getRatId();
        rating.getRatDesc();
        reviews.getrComment();
        reviews.getrId();
    }
    
        int actRec()
        {
            int actRecord = 8;
            switch(actRecord)
            {
                case 1:
                    System.out.println(getAct().getActName());
                    if(getStud().getStuId() > 4)
                    {
                        System.out.println("You are not able to enter record anymore...");
                    }
                    break;
                default :
                {
                    System.out.println("Go to next step");
                }
                actRecord = actRecord - 8;
                            
            }
        return actRecord;
        }
    
// 		double ticketPrice = 500; //Initial ticket price is $10
// 		double discount = 0;
// 		
// 		//Ticket price for Customers under 18 is $7
// 		if(stud.getStuAge() <= 9){ 
//			discount = 70;
//		}
//		//Students between 10 and 25 get a 15% discount
//                else if(stud.getStuAge() >= 10 & stud.getStuAge() <= 25){
//			discount = 350;
//		}
//		//Students over 25 get a 10% discount
//		else if (stud.getStuAge() <= 26)
//		{
//			discount = 240;
//		}
//		//People who are 65+ who are not students get a 7% discount
//                else if(stud.getStuAge() <= 64)
//		{
//			discount = 150;
//		}
//		
//		
//		return ticketPrice = ticketPrice - discount;
        double price() {
                double ticketPrice = 500; //Initial ticket price is $10
 		double discount = 0;
 		
 		//Ticket price for Customers under 9 is $7
 		if(getStud().getStuAge() <= 9){ 
			discount = 70;
		}
		//Students between 10 and 25 get a 15% discount
                else if(getStud().getStuAge() >= 10 & getStud().getStuAge() < 25){
			discount = 75;
		}
//		//Students over 25 get a 10% discount
		else if (getStud().getStuAge() <= 26)
		{
			discount = 50;
		}
		//People who are 65+ who are not students get a 7% discount
		if(getStud().getStuAge() >= 64)
		{
			discount = 35;
		}
		ticketPrice = (ticketPrice - discount);
		
		return ticketPrice;
	}
    
    
    
    /**
     * @return the stud
     */
    public StudentClass getStud() {
        return stud;
    }

    /**
     * @param stud the stud to set
     */
    public void setStud(StudentClass stud) {
        this.stud = stud;
    }
    
    /**
     * @return the act
     */
    public ExcercisesClass getAct() {
        return act;
    }
 
    /**
     * @param act the act to set
     */
    public void setAct(ExcercisesClass act) {
        this.act = act;
    }
    
    /**
     * @return the timeact
     */
    public TimetableClass getTimeact() {
        return timeact;
    }

    /**
     * @param timeact the timeact to set
     */
    public void setTimeact(TimetableClass timeact) {
        this.timeact = timeact;
    }
    
     /**
     * @return the rating
     */
    public RatingClass getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(RatingClass rating) {
        this.rating = rating;
    }
    
    /**
     * @return the reviews
     */
    public ReviewClass getReviews() {
        return reviews;
    }

    /**
     * @param reviews the reviews to set
     */
    public void setReviews(ReviewClass reviews) {
        this.reviews = reviews;
    }
    
    @Override
    public String toString()
    {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        return "--------------------------------------------\n"
                + "Booking Detail for (DAY): -  " +timeact.getActDay()+ "   " + "Activity Shift: - "+ " " +timeact.getActShift()+") \n"
                + "Student: - " +stud.getStuId()+ "  " + "Student Name: - "+ " " +stud.getStuName()+ " " + "Student Age: - "+ " " + stud.getStuAge()+") \n"
                + "Activity Detail: - " +act.getActName()+ " " + actRec()+ " " + "Price: - " +decimalFormat.format(price())+") \n"
                + "Rating Detail: - " +getRating().getRatId()+ "  " + "Rating Desc: - " +getRating().getRatDesc()+") \n"
                + "Review Deail: -" +reviews.getrId()+ " " + "Review Comment: - " +reviews.getrComment()+") \n";
               
    }  

    

   
}
