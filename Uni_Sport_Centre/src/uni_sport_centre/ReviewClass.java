/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

/**
 *
 * @author harshil.janu.solanki
 */
public class ReviewClass {
    //private int score;
    private int rId;
    private String rComment;
    
    
    public ReviewClass(int rId, String rComment)
    {
        this.rId = rId;
        this.rComment = rComment;
    }

    public ReviewClass() {
        this.rId = 1;
        this.rComment = "Anonymous"; 
    }

    /**
     * @return the rId
     */
    public int getrId() {
        return rId;
    }

    /**
     * @param rId the rId to set
     */
    public void setrId(int rId) {
        this.rId = rId;
    }

    /**
     * @return the rComment
     */
    public String getrComment() {
        return rComment;
    }

    /**
     * @param rComment the rComment to set
     */
    public void setrComment(String rComment) {
        this.rComment = rComment;
    }
}
