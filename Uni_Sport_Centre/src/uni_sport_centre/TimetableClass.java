/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

/**
 *
 * @author harshil.janu.solanki
 */
public class TimetableClass {
    private String actShift;
    private String actDay;
    
    public TimetableClass(String actShift, String actDay)
    {
        this.setActShift(actShift);
        this.setActDay(actDay);
    }
    
    public TimetableClass()
    {
        this.actDay = "Anonymous";
        this.actShift = "Anonymous";
    }

    /**
     * @return the actShift
     */
    public String getActShift() {
        return actShift;
    }

    /**
     * @param actShift the actShift to set
     */
    public void setActShift(String actShift) {
        this.actShift = actShift;
    }

    /**
     * @return the actDay
     */
    public String getActDay() {
        return actDay;
    }

    /**
     * @param actDay the actDay to set
     */
    public void setActDay(String actDay) {
        this.actDay = actDay;
    }
    
    @Override
    public String toString()
    {
        return "Shifts"+ " " +this.actShift+ "  " + "Days"+ " " +this.actDay;
        
    }    
}
