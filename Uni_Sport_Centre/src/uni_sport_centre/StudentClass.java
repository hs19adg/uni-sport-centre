/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

/**
 *
 * @author harshil.janu.solanki
 */
public class StudentClass {
     private int stuId;
    private String stuName;
    private int stuAge;
    
    
    public StudentClass(int sId, String sName, int sAge)
    {
        this.stuId = sId;
        this.stuName = sName;
        this.stuAge = sAge;
    }

    public StudentClass() {
        this.stuId = '0';
        this.stuName = "Anonymous";
        this.stuAge = -1;
    }

    /**
     * @return the stuId
     */
    public int getStuId() {
        return stuId;
    }

    /**
     * @param stuId the stuId to set
     */
    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    /**
     * @return the stuName
     */
    public String getStuName() {
        return stuName;
    }

    /**
     * @param stuName the stuName to set
     */
    public void setStuName(String stuName) {
        this.stuName = stuName;
    }
    
    /**
     * @return the stuAge
     */
    public int getStuAge() {
        return stuAge;
    }

    /**
     * @param stuAge the stuAge to set
     */
    public void setStuAge(int stuAge) {
        this.stuAge = stuAge;
    }
    
    public String toString()
    {
        return "Student ID: "+ " " +this.getStuId()+ "Student Name "+ " " +this.getStuName()+ "Student Age "+ " " +this.getStuAge();
    }

   

   
   
}
