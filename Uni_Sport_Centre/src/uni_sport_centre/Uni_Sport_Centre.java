/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

import java.util.Scanner;
import java.util.Arrays;


/**
 *
 * @author harshil.janu.solanki
 */
public class Uni_Sport_Centre {

    
    static Scanner scan = new Scanner(System.in);
    static StudentClass studexc = new StudentClass();
    static ReviewClass reviewexc = new ReviewClass();
    static final int NUMBER_OF_ACTIVITIES = 8;
    static final int NUMBER_OF_SHIFTS = 48;
    static final int NUMBER_OF_RATING = 5;
    static final int NUMBER_OF_STUDENTS = 4;
    
    static ExcercisesClass[] exce = new ExcercisesClass[NUMBER_OF_ACTIVITIES];
    static TimetableClass[] acttimetab1 = new TimetableClass[NUMBER_OF_SHIFTS];
    static RatingClass[] ratingrecord = new RatingClass[NUMBER_OF_RATING];
    static StudentClass[] studex = new StudentClass[NUMBER_OF_STUDENTS];
    
    
    
    
    //TimetableClass timetableDesired = new TimetableClass();
    
    ExcercisesClass desiredAct = new ExcercisesClass();
    TimetableClass desiredTimetable = new TimetableClass();
    RatingClass ratingreacords = new RatingClass();
    //static ArrayList<Activitytype> actExc;
    /**
     * @param args the command line arguments
     */
    private static TimetableClass timetableInput()
    {
        for(int count =0; count< NUMBER_OF_SHIFTS; count++)
        {
            System.out.println((count + 1) + "." + acttimetab1[count]);
        }
        System.out.println("Which shifts and days whould like to select?");
        int selection1 = scan.nextInt();
        selection1 = selection1 - 1;
        
        return acttimetab1[selection1];
    }
    
    private static StudentClass studentInput()
    {
        
        
        
	
        
        //String sname = "";
        
        
//        System.out.println("Enter your id: -");
//        sid = scan.nextC
//        studexc.setStuId(sid);
        
        
        System.out.println("Enter your Name: - ");
        String sname = scan.next();
        studexc.setStuName(sname);
        
        
        System.out.println("Enter your Age: - ");
        int sage = scan.nextInt();
        
        while(sage < 0 || sage > 70 || sage != (int)sage)
        {
            sage = -1;
            System.out.println("Please Enter Valid Age: - ");
            sage = scan.nextInt();
        }
        studexc.setStuAge(sage);
        
        System.out.println("Please Enter your ID ");
        int sid = 0;
        sid = scan.nextInt();
        studexc.setStuId(sid);
        
		
	
	//while (sid !=  && sid != 'n');
	
        
        return studexc;
    }
    
    private static ExcercisesClass excerciseInput()
    {
        //actExc = new ArrayList<Activitytype>();
        for(int count =0; count < NUMBER_OF_ACTIVITIES; count++)
        {
            System.out.println((count + 1) + "." + exce[count]);
        }
        System.out.println("Which activity would you like to select?");
        int selection = scan.nextInt();
        selection = selection - 1 ;
        
        return exce[selection];
        
    }
    
    private static RatingClass ratingInput()
    {
        for(int count = 0; count < NUMBER_OF_RATING; count++)
        {
            System.out.println((count + 1) + "." + ratingrecord[count]);
        }
        System.out.println("Please give rating what you want.");
        int selection = scan.nextInt();
       
        selection = selection - 1 ;
        
        return ratingrecord[selection];
        
    }
    
    private static ReviewClass reviewInput()
    {
        int reviewId = 0;
        String reviewAct = "Anonymous";        

        
//        System.out.println("Enter Review Id: -");
//        reviewId = scan.nextInt();
//        reviewexc.setrId(reviewId);
        
        
        
        do
        {
		System.out.println("Do you want to give review? (Type Y or N): ");
		reviewId = scan.next().charAt(0);
		reviewId = Character.toLowerCase(reviewId);
	}
	while (reviewId != 'y' && reviewId != 'n');
	reviewexc.setrId(reviewId);
        
        System.out.println("Enter Review Comment: -");
        reviewAct = scan.next();
        reviewexc.setrComment(reviewAct);
        
        return reviewexc;
        
    }
    
    
    
    
    
    
    private static BookingClass issuebooking(TimetableClass aTimetab, StudentClass aStudent, ExcercisesClass aActivity, RatingClass aRating, ReviewClass aReview)
    {
        BookingClass bookingclass = new BookingClass(aStudent,aActivity,aTimetab,aRating,aReview);
//        aStudent.getStuId();
//        aStudent.getStuName();
        //bookingclass.notify();
        System.out.println(bookingclass);
        //int studrecordexce = 4;
        int maximumstud = 4;
        while(maximumstud < studex.length)
        {
            StudentClass studentdata = studex[maximumstud];
            if(studentdata!=null)
            {
                System.out.println(studentdata.getStuName());
                maximumstud = maximumstud + 1;
            }
        }
        
        
        int minimumAge = 2;
		 
		 //Makes sure the student is old enough to view the movie.
		if (aStudent.getStuAge() >= minimumAge) {
			return bookingclass;
		}
		else {
			System.out.println("Sorry, You are not old enough for this Activity. \n");
			return null;
		}
        //Make sure only 4 students can alegible to access each activity class.
                
                
        //aActivity.getActId();
//        aActivity.getActName();
//        aActivity.getActPrice();
//        int minId = 1;
//        
//        if(aStudent.getStuId() > minId )
//        {
//            return bookingclass;
//        }
        //return bookingclass;
        
    }
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        //Timetable means(Days and Shifts) records
        
        
        TimetableClass Shift1 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift2 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift3 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift4 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift5 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift6 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift7 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift8 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift9 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift10 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift11 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift12 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift13 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift14 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift15 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift16 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift17 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift18 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift19 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift20 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift21 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift22 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift23 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift24 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift25 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift26 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift27 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift28 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift29 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift30 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift31 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift32 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift33 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift34 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift35 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift36 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift37 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift38 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift39 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift40 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift41 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift42 = new TimetableClass("EVENING "," SUNDAY");
        TimetableClass Shift43 = new TimetableClass("MORNING "," SATURDAY");
        TimetableClass Shift44 = new TimetableClass("AFTERNOON "," SATURDAY");
        TimetableClass Shift45 = new TimetableClass("EVENING "," SATURDAY");
        TimetableClass Shift46 = new TimetableClass("MORNING "," SUNDAY");
        TimetableClass Shift47 = new TimetableClass("AFTERNOON "," SUNDAY");
        TimetableClass Shift48 = new TimetableClass("EVENING "," SUNDAY");
        
        
        //calling arrays
        acttimetab1[0] = Shift1;
        acttimetab1[1] = Shift2;
        acttimetab1[2] = Shift3;
        acttimetab1[3] = Shift4;
        acttimetab1[4] = Shift5;
        acttimetab1[5] = Shift6;
        acttimetab1[6] = Shift7;
        acttimetab1[7] = Shift8;
        acttimetab1[8] = Shift9;
        acttimetab1[9] = Shift10;
        acttimetab1[10] = Shift11;
        acttimetab1[11] = Shift12;
        acttimetab1[12] = Shift13;
        acttimetab1[13] = Shift14;
        acttimetab1[14] = Shift15;
        acttimetab1[15] = Shift16;
        acttimetab1[16] = Shift17;
        acttimetab1[17] = Shift18;
        acttimetab1[18] = Shift19;
        acttimetab1[19] = Shift20;
        acttimetab1[20] = Shift21;
        acttimetab1[21] = Shift22;
        acttimetab1[22] = Shift23;
        acttimetab1[23] = Shift24;
        acttimetab1[24] = Shift25;
        acttimetab1[25] = Shift26;
        acttimetab1[26] = Shift27;
        acttimetab1[27] = Shift28;
        acttimetab1[28] = Shift29;
        acttimetab1[29] = Shift30;
        acttimetab1[30] = Shift31;
        acttimetab1[31] = Shift32;
        acttimetab1[32] = Shift33;
        acttimetab1[33] = Shift34;
        acttimetab1[34] = Shift35;
        acttimetab1[35] = Shift36;
        acttimetab1[36] = Shift37;
        acttimetab1[37] = Shift38;
        acttimetab1[38] = Shift39;
        acttimetab1[39] = Shift40;
        acttimetab1[40] = Shift41;
        acttimetab1[41] = Shift42;
        acttimetab1[42] = Shift43;
        acttimetab1[43] = Shift44;
        acttimetab1[44] = Shift45;
        acttimetab1[45] = Shift46;
        acttimetab1[46] = Shift47;
        acttimetab1[47] = Shift48;
        
        
        //Excercise/Activity records
        ExcercisesClass Yoga = new ExcercisesClass("YOGA ");
        ExcercisesClass Boxing = new ExcercisesClass("BOXING ");
        ExcercisesClass Cricket = new ExcercisesClass("CRICKET ");
        ExcercisesClass Voleyball = new ExcercisesClass("VOLEYBALL ");
        ExcercisesClass Football = new ExcercisesClass("FOOTBALL ");
        ExcercisesClass Zumba = new ExcercisesClass("ZUMBA ");
        ExcercisesClass Airobics = new ExcercisesClass("AROBICS ");
        ExcercisesClass Carrom = new ExcercisesClass("CARROM ");
        
        //calling arrays
        exce[0] = Yoga;
        exce[1] = Boxing;
        exce[2] = Cricket;
        exce[3] = Voleyball;
        exce[4] = Football;
        exce[5] = Zumba;
        exce[6] = Airobics;
        exce[7] = Carrom;
        
        
        RatingClass rate1 = new RatingClass(1,"Very Dissatisfied");
        RatingClass rate2 = new RatingClass(2,"Dissatisfied");
        RatingClass rate3 = new RatingClass(3,"Ok");
        RatingClass rate4 = new RatingClass(4,"Satisfied");
        RatingClass rate5 = new RatingClass(5,"Very Satisfied");
        
        ratingrecord[0] = rate1;
        ratingrecord[1] = rate2;
        ratingrecord[2] = rate3;
        ratingrecord[3] = rate4;
        ratingrecord[4] = rate5;
        
        boolean bookAgainLoop = true;
        do{
            System.out.println(issuebooking(timetableInput(), studentInput(), excerciseInput(), ratingInput(), reviewInput()));
            //System.out.println(excerciseInput().toString());
            char bookAgain ='0';
            while(bookAgain != 'Y' && bookAgain != 'N' )
            {
                System.out.println("Would you like to add more students in the same activity? (Y / N)");
                bookAgain = scan.next().charAt(0);
                bookAgain = Character.toUpperCase(bookAgain);
                if(bookAgain == 'N')
                {
                    System.out.println("Come Next Time! Bye!..");
                    bookAgainLoop = false; //Close the services from here...
                }
                else if(bookAgain == 'Y')
                {
                    System.out.println(studexc.toString());
                    System.out.println(exce.toString());
                    
//                    System.out.println();
//                    System.out.println(studentInput().toString().length());
//                    System.out.println(excerciseInput().toString().length());
                    
                    //System.out.println(excerciseInput().toString());
                    //System.out.println(excerciseInput().toString());
                    continue;//Looping the program...
                    
                }
            }
        }
        while(bookAgainLoop);
        //System.exit(0);
        
    }

    
}
