/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author harshil.janu.solanki
 */
public class StudentClassTest {
    
    public StudentClassTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getStuId method, of class StudentClass.
     */
    @Test
    public void testGetStuId() {
        System.out.println("getStuId");
        StudentClass instance = new StudentClass();
        char expResult = '0';
        int result = instance.getStuId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setStuId method, of class StudentClass.
     */
    @Test
    public void testSetStuId() {
        System.out.println("setStuId");
        char stuId = '0';
        StudentClass instance = new StudentClass();
        instance.setStuId(stuId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getStuName method, of class StudentClass.
     */
    @Test
    public void testGetStuName() {
        System.out.println("getStuName");
        StudentClass instance = new StudentClass();
        String expResult = "Anonymous";
        String result = instance.getStuName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setStuName method, of class StudentClass.
     */
    @Test
    public void testSetStuName() {
        System.out.println("setStuName");
        String stuName = "Anonymous";
        StudentClass instance = new StudentClass();
        instance.setStuName(stuName);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getStuAge method, of class StudentClass.
     */
    @Test
    public void testGetStuAge() {
        System.out.println("getStuAge");
        StudentClass instance = new StudentClass();
        int expResult = -1;
        int result = instance.getStuAge();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setStuAge method, of class StudentClass.
     */
    @Test
    public void testSetStuAge() {
        System.out.println("setStuAge");
        int stuAge = 0;
        StudentClass instance = new StudentClass();
        instance.setStuAge(stuAge);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toStriing method, of class StudentClass.
     */
    @Test
    public void testToStriing() {
        System.out.println("toStriing");
        StudentClass instance = new StudentClass();
        instance.setStuAge(0);
        instance.setStuId('0');
        instance.setStuName("Anonymous");
        String result = instance.toString();
        boolean isNotEmpty = result.length()>0;
        assertEquals(true, isNotEmpty);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
