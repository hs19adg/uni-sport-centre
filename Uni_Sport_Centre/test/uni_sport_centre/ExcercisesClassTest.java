/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni_sport_centre;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author harshil.janu.solanki
 */
public class ExcercisesClassTest {
    
    public ExcercisesClassTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getActName method, of class ExcercisesClass.
     */
    @Test
    public void testGetActName() {
        System.out.println("getActName");
        ExcercisesClass instance = new ExcercisesClass();
        String expResult = "Anonymous";
        String result = instance.getActName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setActName method, of class ExcercisesClass.
     */
    @Test
    public void testSetActName() {
        System.out.println("setActName");
        String actName = "Anonymous";
        boolean usingAct = false;
        ExcercisesClass instance = new ExcercisesClass();
        instance.setActName(actName , usingAct);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        //assertEquals(instance.getActName());
    }

    /**
     * Test of toString method, of class ExcercisesClass.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ExcercisesClass instance = new ExcercisesClass();
        instance.setActName("Anonymous", true);
        String result = instance.toString();
        boolean isNotEmpty = result.length()>0;
        assertEquals(true, isNotEmpty);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
